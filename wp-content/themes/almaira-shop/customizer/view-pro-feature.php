<?php 
/**
 * View Pro Option.
 *
* @package ThemeHunk
 * @subpackage Almaira Shop
 * @since 1.0.0
 */


$wp_customize->add_setting('almaira_shop_pro_one', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_one',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Drag & Drop Section Ordering', 'almaira-shop' )),
        'priority'   =>1,
    )));
$wp_customize->add_setting('almaira_shop_pro_ntn', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_ntn',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>Advance Typography', 'almaira-shop' )),
        'priority'   =>2
    )));
$wp_customize->add_setting('almaira_shop_pro_thr', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_thr',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Off Canvas Filter ', 'almaira-shop' )),
        'priority'   =>3
    )));


$wp_customize->add_setting('almaira_shop_pro_fitn', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_fitn',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>Full Color Control', 'almaira-shop' )),
        'priority'   =>4
    )));

$wp_customize->add_setting('almaira_shop_pro_two', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_two',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> More Header Layout', 'almaira-shop' )),
        'priority'   =>5,
    )));
$wp_customize->add_setting('almaira_shop_pro_five', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_five',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>More Footer Layout', 'almaira-shop' )),
        'priority'   =>6,
    )));


$wp_customize->add_setting('almaira_shop_pro_sixtn', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_sixtn',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>About Us Template', 'almaira-shop' )),
        'priority'   =>7
    )));
$wp_customize->add_setting('almaira_shop_pro_egtn', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_egtn',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>Team Template', 'almaira-shop' )),
        'priority'   =>8
    )));


$wp_customize->add_setting('almaira_shop_pro_sevtn', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_sevtn',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>FAQ Template', 'almaira-shop' )),
        'priority'   =>9
    )));

$wp_customize->add_setting('almaira_shop_pro_eight', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_eight',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Brand Section', 'almaira-shop' )),
        'priority'   =>10
    )));

$wp_customize->add_setting('almaira_shop_pro_seven', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_seven',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Service Section', 'almaira-shop' )),
        'priority'   =>11
    )));


$wp_customize->add_setting('almaira_shop_pro_nine', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_nine',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Four Custom Section', 'almaira-shop' )),
        'priority'   =>12
    )));

$wp_customize->add_setting('almaira_shop_pro_ten', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_ten',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Product Slider Widget', 'almaira-shop' )),
        'priority'   =>12
    )));
$wp_customize->add_setting('almaira_shop_pro_elv', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_elv',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Category Product Slider Widget', 'almaira-shop' )),
        'priority'   =>13
    )));

$wp_customize->add_setting('almaira_shop_pro_tlw', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_tlw',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Floating Cart ', 'almaira-shop' )),
        'priority'   =>14
    )));
$wp_customize->add_setting('almaira_shop_pro_six', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_six',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Fixed Footer', 'almaira-shop' )),
        'priority'   =>15,
    )));

$wp_customize->add_setting('almaira_shop_pro_frty', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_frty',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span> Loadmore & Infinite Scroll', 'almaira-shop' )),
        'priority'   =>16
    )));

$wp_customize->add_setting('almaira_shop_pro_twnty', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_twnty',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>Copyright', 'almaira-shop' )),
        'priority'   =>17
    )));


$wp_customize->add_setting('almaira_shop_pro_three', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_three',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( __( '<span class="view-pro">Pro</span> <span class="pro-desc"></span>Header transparency', 'almaira-shop' )),
        'priority'   =>18,
    )));


$wp_customize->add_setting('almaira_shop_pro_get', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));


$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_pro_get',
            array(
        'section'     => 'almaira-shop-view-pro',
        'type'        => 'custom_message',
        'description' => sprintf( wp_kses(__( '<a class="get-pro-version" target="_blank" href="%s">Get The Pro Version !</a>', 'almaira-shop' ), array(  'a' => array( 'href' => array(),'target' => array() ) ) ), esc_url('https://themehunk.com/almaira-pro/')),
        'priority'   =>30,
    )));