<?php
/**
 * Transparent Header for  Almaira Shop Theme.
* @package ThemeHunk
 * @subpackage Almaira Shop
 * @since 1.0.0
 */

/***********************/
//sticky header
/***********************/

//main header
$wp_customize->add_setting( 'almaira_stick_main_header_active', array(
                'default'               => false,
                'sanitize_callback'     => 'almaira_shop_sanitize_checkbox',
            ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'almaira_stick_main_header_active', array(
                'label'                 => esc_html__('Stick Main Header', 'almaira-shop'),
                'type'                  => 'checkbox',
                'section'               => 'almaira-shop-sticky-header',
                'settings'              => 'almaira_stick_main_header_active',
            ) ) );
// animation
$wp_customize->add_setting('almaira_shop_stick_animation', array(
        'default'        => 'fade',
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'esc_attr',
    ));
$wp_customize->add_control('almaira_shop_stick_animation', array(
        'settings' => 'almaira_shop_stick_animation',
        'label'    => __('Select Animation Effect','almaira-shop'),
        'section'  => 'almaira-shop-sticky-header',
        'type'     => 'select',
        'choices'  => array(
        'fade'     => __('Fade','almaira-shop'),
        'slide'    => __('Slide','almaira-shop'), 
    ),
));
/****************/
//doc link
/****************/
$wp_customize->add_setting('almaira_shop_sicky_header_doc_learn_more', array(
    'sanitize_callback' => 'almaira_shop_sanitize_text',
    ));
$wp_customize->add_control(new Almaira_Shop_Misc_Control( $wp_customize, 'almaira_shop_sicky_header_doc_learn_more',
            array(
        'section'     => 'almaira-shop-sticky-header',
        'type'        => 'custom_message',
        'description' => sprintf( wp_kses(__( 'Sticky header will fix your header on the top, whenever you scroll the site, it will scroll down with it. To know more go with this <a target="_blank" href="%s">Doc</a> !', 'almaira-shop' ), array(  'a' => array( 'href' => array(),'target' => array() ) ) ), esc_url('https://themehunk.com/docs/almaira-shop-theme/#sticky-header')),
         'priority'   =>50,
    )));